#!/bin/bash
baseurl="https://storage.googleapis.com/golang/"
curl https://golang.org/dl/ -o index.html && \
dlfile=$(cat index.html | grep "linux-amd64" | grep "download downloadBox" | head -n 1 | awk -F/ '{ print $5 }' | awk -F'"' '{ print $1 }') && \
echo dlfile=${dlfile}
curl -vL ${baseurl}${dlfile} -o ${dlfile} && \
tar xvfz ${dlfile} && \
rm *.tar.gz index.html